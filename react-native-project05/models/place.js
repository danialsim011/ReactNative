class Place {
	constructor(placeId, placeTitle, placeImageUri, placeAddress, placeLatitude, placeLongitude) {
		this.placeId = placeId;
		this.placeTitle = placeTitle;
		this.placeImageUri = placeImageUri;
		this.placeAddress = placeAddress;
		this.placeLatitude = placeLatitude;
		this.placeLongitude = placeLongitude;
	}
}

export default Place;
