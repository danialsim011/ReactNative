import React, { useEffect } from "react";
import { FlatList, View, Text, StyleSheet, Platform } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";

import HeaderButton from "../components/HeaderButton";
import PlaceItem from "../components/PlaceItem";
import * as placeActions from "../store/place-action";

const PlaceListScreen = (props) => {
	const places = useSelector((state) => state.place.places);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(placeActions.fetchPlace());
	}, [dispatch]);

	if (places.length === 0) {
		return (
			<View style={styles.centered}>
				<Text>No places added yet. Let's start adding!</Text>
			</View>
		);
	}

	return (
		<FlatList
			data={places}
			keyExtractor={(item) => item.placeId}
			renderItem={(itemData) => (
				<PlaceItem
					image={itemData.item.placeImageUri}
					title={itemData.item.placeTitle}
					address={itemData.item.placeAddress}
					onSelect={() => {
						props.navigation.navigate("PlaceDetail", {
							placeTitle: itemData.item.placeTitle,
							placeId: itemData.item.placeId,
						});
					}}
				/>
			)}
		/>
	);
};

PlaceListScreen.navigationOptions = (navigationData) => {
	return {
		headerTitle: "All Places",
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Add Place"
					iconName={Platform.OS === "android" ? "md-add" : "ios-add"}
					onPress={() => {
						navigationData.navigation.navigate("NewPlace");
					}}
				/>
			</HeaderButtons>
		),
	};
};

const styles = StyleSheet.create({
	centered: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default PlaceListScreen;
