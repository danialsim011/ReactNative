import React, { useCallback, useState } from "react";
import { ScrollView, View, Button, Text, TextInput, StyleSheet } from "react-native";
import { useDispatch } from "react-redux";

import Colors from "../constants/Colors";
import ImageSelector from "../components/ImageSelector";
import LocationPicker from "../components/LocationPicker";
import * as placeActions from "../store/place-action";

const NewPlaceScreen = (props) => {
	const [placeTitle, setPlaceTitle] = useState("");
	const [placeImage, setPlaceImage] = useState(null);
	const [placeLocation, setPlaceLocation] = useState(null);

	const dispatch = useDispatch();

	const placeTitleChangedHandler = (text) => {
		/* May add validation here if you want to */
		setPlaceTitle(text);
	};

	const placeImageTakenHandler = (imagePath) => {
		setPlaceImage(imagePath);
	};

	const savePlaceHandler = () => {
		dispatch(placeActions.addPlace(placeTitle, placeImage, placeLocation));
		props.navigation.goBack();
	};

	const locationPickedHandler = useCallback((location) => {
		setPlaceLocation(location);
	}, []);

	return (
		<ScrollView>
			<View style={styles.formControl}>
				<Text styles={styles.formLabel}>Title</Text>
				<TextInput
					style={styles.formInput}
					onChangeText={placeTitleChangedHandler}
					value={placeTitle}
				/>
				<ImageSelector onImageTaken={placeImageTakenHandler} />
				<LocationPicker
					navigation={props.navigation}
					onLocationPicked={locationPickedHandler}
				/>
				<Button title="Save Place" color={Colors.primaryColor} onPress={savePlaceHandler} />
			</View>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	formControl: {
		margin: 30,
	},
	formLabel: {
		fontSize: 18,
		marginBottom: 12,
	},
	formInput: {
		borderBottomColor: "#ccc",
		borderBottomWidth: 1,
		marginBottom: 12,
		paddingVertical: 4,
		paddingHorizontal: 4,
	},
});

NewPlaceScreen.navigationOptions = () => {
	return {
		headerTitle: "Add Place",
	};
};

export default NewPlaceScreen;
