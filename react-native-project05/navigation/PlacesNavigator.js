import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { Platform } from "react-native";

import MapScreen from "../screens/MapScreen";
import NewPlaceScreen from "../screens/NewPlaceScreen";
import PlaceDetailScreen from "../screens/PlaceDetailScreen";
import PlaceListScreen from "../screens/PlaceListScreen";
import Colors from "../constants/Colors";

const PlacesNavigator = createStackNavigator(
	{
		Places: PlaceListScreen,
		PlaceDetail: PlaceDetailScreen,
		NewPlace: NewPlaceScreen,
		Map: MapScreen,
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: Platform.OS === "android" ? Colors.primaryColor : "",
			},
			headerTintColor: Platform.OS === "android" ? "white" : Colors.primaryColor,
		},
	}
);

export default createAppContainer(PlacesNavigator);
