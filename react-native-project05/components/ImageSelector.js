import React, { useState } from "react";
import { View, Button, Image, Text, Alert, StyleSheet } from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Camera from "expo-camera";

import Colors from "../constants/Colors";

const ImageSelector = (props) => {
	const [pickedImage, setPickedImage] = useState(null);

	const verifyPermissions = async () => {
		/* This will also return a Promise */
		// const result = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
		/* The CAMERA_ROLL permission is deprecated, hence use the following line instead */
		const result = await Camera.requestCameraPermissionsAsync();
		if (result.status !== "granted") {
			Alert.alert(
				"Insufficient permissions",
				"You need to grant camera permissions to use this app.",
				[{ text: "OK" }]
			);
			return false;
		}
		return true;
	};

	const takeImageHandler = async () => {
		/* Open the camera - this returns a Promise
        Permission required prior to the launch of camera */
		const hasPermission = await verifyPermissions();
		if (!hasPermission) {
			return;
		}
		const imageResult = await ImagePicker.launchCameraAsync({
			allowsEditing: true,
			aspect: [16, 9],
			quality: 0.5,
		});
		setPickedImage(imageResult.uri);
        props.onImageTaken(imageResult.uri);
	};

	return (
		<View style={styles.imageSelector}>
			<View style={styles.imagePreview}>
				{!pickedImage ? (
					<Text>No image picked yet.</Text>
				) : (
					<Image style={styles.image} source={{ uri: pickedImage }} />
				)}
			</View>
			<Button title="Take Image" color={Colors.primaryColor} onPress={takeImageHandler} />
		</View>
	);
};

const styles = StyleSheet.create({
	imageSelector: {
		alignItems: "center",
        marginBottom: 16,
	},
	imagePreview: {
		width: "100%",
		height: 240,
		marginBottom: 12,
		justifyContent: "center",
		alignItems: "center",
		borderColor: "#ccc",
		borderWidth: 1,
	},
	image: {
		width: "100%",
		height: "100%",
	},
});

export default ImageSelector;
