import React, { useEffect, useState } from "react";
import { View, Button, Text, ActivityIndicator, Alert, StyleSheet } from "react-native";
import * as Location from "expo-location";

import MapPreview from "./MapPreview";
import Colors from "../constants/Colors";

const LocationPicker = (props) => {
	const [isFetchingLocation, setIsFetchingLocation] = useState(false);
	const [placeLocation, setPlaceLocation] = useState(null);

    const mapPickedLocation = props.navigation.getParam("pickedLocation");
    const { onLocationPicked } = props;

    useEffect(() => {
        if (mapPickedLocation) {
            setPlaceLocation(mapPickedLocation);
            onLocationPicked(mapPickedLocation);
        }
    }, [mapPickedLocation, onLocationPicked]);

	const verifyPermissions = async () => {
		/* This will also return a Promise */
		// const result = await Permissions.askAsync(Permissions.LOCATION);
		/* The LOCATION permission is deprecated, hence use the following lines instead */
		const bgResult = await Location.requestBackgroundPermissionsAsync();
		const fgResult = await Location.requestForegroundPermissionsAsync();
		if (bgResult.status !== "granted" && fgResult.status !== "granted") {
			Alert.alert(
				"Insufficient permissions",
				"You need to grant both location (foreground and background) permissions to use this app.",
				[{ text: "OK" }]
			);
			return false;
		}
		return true;
	};

	const getLocationHandler = async () => {
		const hasPermission = await verifyPermissions();
		if (!hasPermission) {
			return;
		}
		setIsFetchingLocation(true);
		try {
			const locationResult = await Location.getCurrentPositionAsync({
				timeout: 5000,
			});
			setPlaceLocation({
				placeLat: locationResult.coords.latitude,
				placeLng: locationResult.coords.latitude,
			});
            props.onLocationPicked({
                placeLat: locationResult.coords.latitude,
				placeLng: locationResult.coords.latitude,
            });
		} catch (error) {
			Alert.alert("Error", "Failed to fetch location.", [{ text: "OK" }]);
		}
		setIsFetchingLocation(false);
	};

	const pickOnMapHandler = async () => {
        props.navigation.navigate("Map");
    };

	return (
		<View style={styles.locationPicker}>
			<MapPreview style={styles.mapPreview} location={placeLocation} onPress={pickOnMapHandler}>
				{isFetchingLocation ? (
					<ActivityIndicator size="large" color={Colors.primaryColor} />
				) : (
					<Text>No location chosen yet.</Text>
				)}
			</MapPreview>
			<View style={styles.actions}>
				<Button
					title="Get Location"
					color={Colors.primaryColor}
					onPress={getLocationHandler}
				/>
				<Button
					title="Pick on Map"
					color={Colors.primaryColor}
					onPress={pickOnMapHandler}
				/>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	locationPicker: {
		marginBottom: 16,
	},
	mapPreview: {
		marginBottom: 8,
		width: "100%",
		height: 160,
		borderColor: "#ccc",
		borderWidth: 1,
	},
	actions: {
		flexDirection: "row",
		justifyContent: "space-around",
		width: "100%",
	},
});

export default LocationPicker;
