import React from "react";
import { TouchableOpacity, Image, StyleSheet } from "react-native";

import env from "../env";

const MapPreview = (props) => {
	const key = env.ggmKey;
	let mapPreviewUrl;
	if (props.location) {
		mapPreviewUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${props.location.latitude},${props.location.longitude}&zoom=14&size=400x160&maptype=roadmap&markers=color:red%7Clabel:A%7C${props.location.latitude},${props.location.longitude}&key=${key}`;
        console.log(mapPreviewUrl);
	}

	return (
		<TouchableOpacity style={{ ...styles.mapPreviewContainer, ...props.style }} onPress={props.onPress}>
			{props.location ? (
				<Image style={styles.mapPreviewImage} source={{ uri: mapPreviewUrl }} />
			) : (
				props.children
			)}
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	mapPreviewContainer: {
		justifyContent: "center",
		alignItems: "center",
	},
	mapPreviewImage: {
		width: "100%",
		height: "100%",
	},
});

export default MapPreview;
