import * as FileSystem from "expo-file-system";

export const ADD_PLACE = "ADD_PLACE";
export const FETCH_PLACE = "FETCH_PLACE";

import { insertPlace, selectPlace } from "../helpers/database";
import env from "../env";

export const addPlace = (placeTitle, placeImageUri, placeLocation) => {
	return async (dispatch) => {
		const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${placeLocation.latitude},${placeLocation.longitude}&key=${env.ggmKey}`;
		console.log(url);
		const placeResponse = await fetch(url);
		if (!placeResponse.ok) {
			throw new Error("Something went wrong");
		}
		const placeResponseData = await placeResponse.json();
		let placeAddress;
		if (!placeResponseData.results) {
			/* Error */
			placeAddress = "Dummy Address";
			throw new Error("Failed to obtain address");
		} else {
			placeAddress = placeResponseData.results[0].formatted_address;
		}
		const imageFileName = placeImageUri.split("/").pop();
		const imageNewPath = FileSystem.documentDirectory + imageFileName;
		try {
			await FileSystem.moveAsync({
				from: placeImageUri,
				to: imageNewPath,
			});
			const dbResultA = await insertPlace(
				placeTitle,
				imageNewPath,
				placeAddress,
				placeLocation.latitude,
				placeLocation.longitude
			);
			dispatch({
				type: ADD_PLACE,
				placeData: {
					placeId: dbResultA.insertId,
					placeTitle: placeTitle,
					placeImageUri: imageNewPath,
					placeAddress: placeAddress,
					placeCoords: {
						placeLatitude: placeLocation.latitude,
						placeLongitude: placeLocation.longitude,
					},
				},
			});
		} catch (errorA) {
			console.log(errorA);
			throw errorA;
		}
	};
};

export const fetchPlace = () => {
	return async (dispatch) => {
		try {
			const dbResultB = await selectPlace();
			dispatch({
				type: FETCH_PLACE,
				places: dbResultB.rows._array,
			});
		} catch (errorB) {
			throw errorB;
		}
	};
};
