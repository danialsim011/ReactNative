import { ADD_PLACE, FETCH_PLACE } from "./place-action";

import Place from "../models/place";

const initialState = {
	places: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case FETCH_PLACE:
			return {
				places: action.places.map(
					(pla) =>
						new Place(
							pla.placeId.toString(),
							pla.placeTitle,
							pla.placeImageUri,
							pla.placeAddress,
							pla.placeLatitude,
							pla.placeLongitude
						)
				),
			};
		case ADD_PLACE:
			const newPlace = new Place(
				action.placeData.placeId.toString(),
				action.placeData.placeTitle,
				action.placeData.placeImageUri,
				action.placeData.placeAddress,
				action.placeData.placeCoords.placeLatitude,
				action.placeData.placeCoords.placeLongitude
			);
			return {
				places: state.places.concat(newPlace),
			};
		default:
			return state;
	}
};
