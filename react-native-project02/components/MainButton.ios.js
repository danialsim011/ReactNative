import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const MainButton = (props) => {
	return (
		<TouchableOpacity activeOpacity={0.5} onPress={props.onPress}>
			<View style={{ ...styles.button, ...props.customStyle }}>
				<Text style={styles.buttonText}>{props.children}</Text>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	button: {
		paddingHorizontal: 12,
		paddingVertical: 8,
		borderRadius: 8,
	},
	buttonText: {
		color: "white",
		fontFamily: "open-sans",
		fontSize: 16,
		alignSelf: "center",
	},
});

export default MainButton;
