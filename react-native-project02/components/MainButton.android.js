import React from "react";
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	TouchableNativeFeedback,
	Platform,
} from "react-native";

const MainButton = (props) => {
	let ButtonComponent = TouchableOpacity;

	if (Platform.Version >= 21) {
		ButtonComponent = TouchableNativeFeedback;
	}

	return (
		<View style={styles.buttonContainer}>
			<ButtonComponent activeOpacity={0.5} onPress={props.onPress}>
				<View style={{ ...styles.button, ...props.customStyle }}>
					<Text style={styles.buttonText}>{props.children}</Text>
				</View>
			</ButtonComponent>
		</View>
	);
};

const styles = StyleSheet.create({
	buttonContainer: {
		borderRadius: 8,
		overflow: "hidden",
	},
	button: {
		paddingHorizontal: 12,
		paddingVertical: 8,
		borderRadius: 8,
	},
	buttonText: {
		color: "white",
		fontFamily: "open-sans",
		fontSize: 16,
		alignSelf: "center",
	},
});

export default MainButton;
