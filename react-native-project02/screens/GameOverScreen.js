import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Image, Dimensions, ScrollView } from "react-native";

import MainButton from "../components/MainButton";
import Colors from "../constants/colors";
import DefaultStyles from "../constants/default-styles";

const GameOverScreen = (props) => {
	const [availableDeviceWidth, setAvailableDeviceWidth] = useState(
		Dimensions.get("window").width
	);
	const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
		Dimensions.get("window").height
	);

	useEffect(() => {
		const updateLayout = () => {
			setAvailableDeviceWidth(Dimensions.get("window").width);
			setAvailableDeviceHeight(Dimensions.get("window").height);
		};

		Dimensions.addEventListener("change", updateLayout);

		return () => {
			Dimensions.removeEventListener("change", updateLayout);
		};
	});

	return (
		<ScrollView>
			<View style={styles.screen}>
				<Text style={DefaultStyles.titleText}>Game Over</Text>
				<View
					style={{
						...styles.imageContainer,
						...{
							width: availableDeviceWidth * 0.7,
							height: availableDeviceWidth * 0.7,
							borderRadius: (availableDeviceWidth * 0.7) / 2,
							marginVertical: availableDeviceHeight / 30,
						},
					}}
				>
					<Image
						style={styles.image}
						source={require("../assets/success.png")}
						// source={{uri: "https://cdn.pixabay.com/photo/2018/02/16/10/45/snow-3157374_960_720.jpg"}}
						resizeMode="cover"
					/>
				</View>
				<View
					style={{
						...styles.resultContainer,
						...{
							marginVertical: availableDeviceHeight / 60,
						},
					}}
				>
					<Text
						style={
							(DefaultStyles.bodyText,
							{
								...styles.resultText,
								...{ fontSize: availableDeviceHeight < 400 ? 14 : 18 },
							})
						}
					>
						Your phone needed{" "}
						<Text style={styles.highlightText}>{props.roundsNumber}</Text> rounds to
						guess the number{" "}
						<Text style={styles.highlightText}>{props.userNumber}</Text>.
					</Text>
				</View>
				<View style={styles.button}>
					<MainButton onPress={props.onRestart} customStyle={DefaultStyles.primaryButton}>
						NEW GAME
					</MainButton>
				</View>
			</View>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: 12,
	},
	button: {
		marginTop: 12,
	},
	imageContainer: {
		borderWidth: 3,
		borderColor: "black",
		overflow: "hidden",
	},
	image: {
		width: "100%",
		height: "100%",
	},
	resultContainer: {
		marginHorizontal: 48,
	},
	resultText: {
		textAlign: "center",
	},
	highlightText: {
		color: Colors.primary,
		fontFamily: "open-sans-bold",
	},
});

export default GameOverScreen;
