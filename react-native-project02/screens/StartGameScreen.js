import React, { useState, useEffect } from "react";
import {
	StyleSheet,
	View,
	Text,
	TouchableWithoutFeedback,
	Keyboard,
	Alert,
	ScrollView,
	KeyboardAvoidingView,
	Dimensions,
} from "react-native";

import Card from "../components/Card";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";
import MainButton from "../components/MainButton";
import DefaultStyles from "../constants/default-styles";

const StartGameScreen = (props) => {
	const [enteredNumber, setEnteredNumber] = useState("");
	const [isUserConfirmed, setIsUserConfirmed] = useState(false);
	const [selectedNumber, setSelectedNumber] = useState();
	const [buttonWidth, setButtonWidth] = useState(Dimensions.get("window").width / 4);

	useEffect(() => {
		const updateLayout = () => {
			setButtonWidth(Dimensions.get("window").width / 4);
		};
	
		Dimensions.addEventListener("change", updateLayout);
		return () => {
			Dimensions.removeEventListener("change", updateLayout);
		};
	});

	const numberInputHandler = (inputNumber) => {
		setEnteredNumber(inputNumber.replace(/[^0-9]/g, ""));
	};

	const resetInputHandler = () => {
		setEnteredNumber("");
		setIsUserConfirmed(false);
	};

	const confirmInputHandler = () => {
		const chosenNumber = parseInt(enteredNumber);
		if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99) {
			Alert.alert("Invalid Number", "The number should be between 1 and 99.", [
				{
					text: "OK",
					style: "destructive",
					onPress: resetInputHandler,
				},
			]);
			return;
		}
		setEnteredNumber("");
		setIsUserConfirmed(true);
		setSelectedNumber(chosenNumber);
		Keyboard.dismiss();
	};

	let confirmedOutput;

	if (isUserConfirmed) {
		confirmedOutput = (
			<Card style={styles.summaryContainer}>
				<Text style={DefaultStyles.bodyText}>You selected</Text>
				<NumberContainer style={DefaultStyles.bodyText}>{selectedNumber}</NumberContainer>
				<MainButton
					onPress={() => props.onStartGame(selectedNumber)}
					customStyle={DefaultStyles.primaryButton}
				>
					START GAME
				</MainButton>
			</Card>
		);
	}

	return (
		<ScrollView>
			<KeyboardAvoidingView behavior="position" keyboardVerticalOffset={30}>
				<TouchableWithoutFeedback
					onPress={() => {
						Keyboard.dismiss();
					}}
				>
					<View style={styles.screen}>
						<Text style={styles.title}>Start a new game</Text>
						<Card style={styles.inputContainer}>
							<Text style={DefaultStyles.bodyText}>Select a number</Text>
							<Input
								style={styles.input}
								blurOnSubmit
								autoCapitalize="none"
								autoCorrect={false}
								keyboardType="number-pad"
								maxLength={2}
								onChangeText={numberInputHandler}
								value={enteredNumber}
							/>
							<View style={styles.buttonContainer}>
								<View style={{width: buttonWidth}}>
									<MainButton
										onPress={resetInputHandler}
										customStyle={DefaultStyles.accentButton}
									>
										RESET
									</MainButton>
								</View>
								<View style={{width: buttonWidth}}>
									<MainButton
										onPress={confirmInputHandler}
										customStyle={DefaultStyles.primaryButton}
									>
										CONFIRM
									</MainButton>
								</View>
							</View>
						</Card>
						{confirmedOutput}
					</View>
				</TouchableWithoutFeedback>
			</KeyboardAvoidingView>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		padding: 12,
		alignItems: "center",
	},
	title: {
		fontSize: 16,
		marginVertical: 8,
		fontFamily: "open-sans-bold",
	},
	inputContainer: {
		width: "80%",
		minWidth: 300,
		maxWidth: "90%",
		alignItems: "center",
	},
	buttonContainer: {
		flexDirection: "row",
		width: "100%",
		justifyContent: "space-between",
		paddingHorizontal: 12,
	},
	input: {
		width: 60,
		textAlign: "center",
	},
	summaryContainer: {
		marginTop: 20,
		alignItems: "center",
	},
});

export default StartGameScreen;
