class CartItem {
    constructor (quantity, productUnitPrice, productTitle, pushToken, subtotal) {
        this.productTitle = productTitle;
        this.productUnitPrice = productUnitPrice;
        this.quantity = quantity;
        this.pushToken = pushToken;
        this.subtotal = subtotal;
    }
}

export default CartItem;