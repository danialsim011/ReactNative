import { AUTHENTICATE_USER, LOGOUT_USER, SET_DID_TRY_AUTO_LOGIN } from "../actions/auth-actions";

const initialState = {
    userToken: null,
    userId: null,
    didTryAutoLogin: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTHENTICATE_USER:
            return {
                userToken: action.userToken,
                userId: action.userId,
                didTryAutoLogin: true,
            };
        case SET_DID_TRY_AUTO_LOGIN:
            return {
                ...state,
                didTryAutoLogin: true,
            }
        case LOGOUT_USER:
            return {
                ...initialState,
                didTryAutoLogin: true,
            };
        default:
            return state;
    }
};