import Order from "../../models/order";

export const ADD_ORDER = "ADD_ORDER";
export const FETCH_ORDER = "FETCH_ORDER";

export const fetchOrder = () => {
	return async (dispatch, getState) => {
		/* Execute any async code here */
		const userId = getState().auth.userId;
		try {
			const response = await fetch(
				`https://flutter-dart-12345-default-rtdb.asia-southeast1.firebasedatabase.app/orders/${userId}.json`
			);
			if (!response.ok) {
				throw new Error("Something went wrong.");
			}
			const responseData = await response.json();
			let loadedOrders = [];
			for (const key in responseData) {
				loadedOrders.push(
					new Order(
						key,
						responseData[key].cartItems,
						responseData[key].totalAmount,
						new Date(responseData[key].date)
					)
				);
			}
			dispatch({ type: FETCH_ORDER, orders: loadedOrders });
		} catch (error) {
			throw error;
		}
	};
};

export const addOrder = (cartItems, totalAmount) => {
	return async (dispatch, getState) => {
		/* Execute any async code here */
		const date = new Date();
		const userToken = getState().auth.userToken;
		const userId = getState().auth.userId;
		const response = await fetch(
			`https://flutter-dart-12345-default-rtdb.asia-southeast1.firebasedatabase.app/orders/${userId}.json?auth=${userToken}`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					cartItems,
					totalAmount: parseFloat(totalAmount.toFixed(2)),
					date: date.toISOString(),
				}),
			}
		);
		if (!response.ok) {
			throw new Error("Something went wrong");
		}
		const responseData = await response.json();
		dispatch({
			type: ADD_ORDER,
			orderData: {
				orderId: responseData.name,
				items: cartItems,
				totalAmount: totalAmount,
				orderDate: date,
			},
		});

		for (const cartItem of cartItems) {
			const pushToken = cartItem.productPushToken;
			fetch("https://exp.host/--api/v2/push/send", {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Accept-Encoding": "gzip, deflate",
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					to: pushToken,
					title: "Order was placed",
					body: cartItem.productTitle,
				}),
			});
		}
	};
};
