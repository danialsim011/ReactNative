import AsyncStorage from "@react-native-async-storage/async-storage";

export const AUTHENTICATE_USER = "AUTHENTICATE_USER";
export const LOGOUT_USER = "LOGOUT_USER";
export const SET_DID_TRY_AUTO_LOGIN = "SET_DID_TRY_AUTO_LOGIN";

let timer;

export const setDidTryAutoLogin = () => {
	return {
		type: SET_DID_TRY_AUTO_LOGIN,
	};
};

export const authenticateUser = (userId, userToken, expirationTime) => {
	return (dispatch) => {
		dispatch(setLogoutTimer(expirationTime));
		dispatch({
			type: AUTHENTICATE_USER,
			userId: userId,
			userToken: userToken,
		});
	};
};

export const signupUser = (userEmail, userPassword) => {
	return async (dispatch) => {
		const response = await fetch(
			"https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBg5Lx5vfENeiqxngeRIbyb5_h1trgF3nw",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					email: userEmail,
					password: userPassword,
					returnSecureToken: true,
				}),
			}
		);
		if (!response.ok) {
			const errorResponseData = await response.json();
			const errorCode = errorResponseData.error.message;
			let errorMessage = "Something went wrong";
			if (errorCode === "EMAIL_EXISTS") {
				errorMessage = "The email has been existed.";
			} else if (errorCode === "TOO_MANY_ATTEMPTS_TRY_LATER") {
				errorMessage = "Unusual activity detected. Please try again later.";
			} else if (errorCode === "OPERATION_NOT_ALLOWED") {
				errorMessage = "Password sign-in is disabled.";
			}
			throw new Error(errorMessage);
		}
		const responseData = await response.json();
		const userId = responseData.localId;
		const userToken = responseData.idToken;
		const expirationTimeInt = parseInt(responseData.expiresIn) * 1000;
		const expirationDate = new Date(new Date().getTime() + expirationTimeInt).toISOString();
		dispatch(authenticateUser(userId, userToken, expirationTimeInt));
		saveAuthDataToStorage(userToken, userId, expirationDate);
	};
};

export const signinUser = (userEmail, userPassword) => {
	return async (dispatch) => {
		const response = await fetch(
			"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBg5Lx5vfENeiqxngeRIbyb5_h1trgF3nw",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					email: userEmail,
					password: userPassword,
					returnSecureToken: true,
				}),
			}
		);
		if (!response.ok) {
			const errorResponseData = await response.json();
			const errorCode = errorResponseData.error.message;
			let errorMessage = "Something went wrong";
			if (errorCode === "EMAIL_NOT_FOUND") {
				errorMessage = "The email could not be found.";
			} else if (errorCode === "INVALID_PASSWORD") {
				errorMessage = "Invalid password.";
			} else if (errorCode === "USER_DISABLED") {
				errorMessage = "The user account has been disabled.";
			}
			throw new Error(errorMessage);
		}
		const responseData = await response.json();
		const userId = responseData.localId;
		const userToken = responseData.idToken;
		const expirationTimeInt = parseInt(responseData.expiresIn) * 1000;
		const expirationDate = new Date(new Date().getTime() + expirationTimeInt).toISOString();
		dispatch(authenticateUser(userId, userToken, expirationTimeInt));
		saveAuthDataToStorage(userToken, userId, expirationDate);
	};
};

export const logoutUser = () => {
	clearLogoutTimer();
	AsyncStorage.removeItem("userData");
	return {
		type: LOGOUT_USER,
	};
};

const saveAuthDataToStorage = (userToken, userId, expirationDate) => {
	AsyncStorage.setItem(
		"userData",
		JSON.stringify({
			userToken: userToken,
			userId: userId,
			expiryDate: expirationDate,
		})
	);
};

const setLogoutTimer = (expirationTime) => {
	return (dispatch) => {
		timer = setTimeout(() => {
			dispatch(logoutUser());
		}, expirationTime);
	};
};

const clearLogoutTimer = () => {
	if (timer) {
		clearTimeout(timer);
	}
};
