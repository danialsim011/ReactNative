import React from "react";
import { FlatList, Button, Alert, Platform } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import ProductItem from "../../components/shop/ProductItem";
import HeaderButton from "../../components/ui/HeaderButton";
import Fallback from "../../components/ui/Fallback";
import Colors from "../../constants/Colors";
import * as productActions from "../../store/actions/product-actions";

const UserProductScreen = (props) => {
	const userProducts = useSelector((state) => state.products.userProducts);
	const dispatch = useDispatch();

	const modifyProductHandler = (productId) => {
		props.navigation.navigate("ModifyProduct", { productId: productId });
	};

	const deleteHandler = (productId) => {
		Alert.alert("Heads up!", "Are you sure you want to delete this item?", [
			{ text: "No", style: "default" },
			{
				text: "Yes",
				style: "destructive",
				onPress: () => {
					dispatch(productActions.deleteProduct(productId));
				},
			},
		]);
	};

	if (userProducts.length === 0) {
		return (
			<Fallback>
				<Text>No products found. Let's start adding some!</Text>
			</Fallback>
		);
	}

	return (
		<FlatList
			data={userProducts}
			keyExtractor={(item) => item.id}
			renderItem={(itemData) => (
				<ProductItem
					title={itemData.item.title}
					unitPrice={itemData.item.unitPrice}
					imageUrl={itemData.item.imageUrl}
					onSelect={() => {
						modifyProductHandler(itemData.item.id);
					}}
				>
					<Button
						color={Colors.primaryColor}
						title="Edit"
						onPress={() => {
							modifyProductHandler(itemData.item.id);
						}}
					/>
					<Button
						color={Colors.primaryColor}
						title="Delete"
						onPress={deleteHandler.bind(this, itemData.item.id)}
					/>
				</ProductItem>
			)}
		/>
	);
};

export const screenOptions = (navigationData) => {
	return {
		headerTitle: "Your Products",
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Menu"
					iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
					onPress={() => {
						navigationData.navigation.toggleDrawer();
					}}
				/>
			</HeaderButtons>
		),
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Add"
					iconName={Platform.OS === "android" ? "md-create" : "ios-create"}
					onPress={() => {
						navigationData.navigation.navigate("ModifyProduct");
					}}
				/>
			</HeaderButtons>
		),
	};
};

export default UserProductScreen;
