import React, { useState, useEffect, useCallback } from "react";
import { FlatList, View, Text, Platform, Button } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import OrderItem from "../../components/shop/OrderItem";
import HeaderButton from "../../components/ui/HeaderButton";
import LoadingSpinner from "../../components/ui/LoadingSpinner";
import Fallback from "../../components/ui/Fallback";
import * as orderActions from "../../store/actions/order-actions";
import Colors from "../../constants/Colors";

const OrderScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [isRefreshing, setIsRefreshing] = useState(false);
	const [error, setError] = useState(null);
	const orders = useSelector((state) => state.order.orders);
	const dispatch = useDispatch();

	const loadOrders = useCallback(async () => {
		setError(null);
		setIsRefreshing(true);
		try {
			await dispatch(orderActions.fetchOrder());
		} catch (error) {
			setError(error.message);
		}
		setIsRefreshing(false);
	}, [dispatch, setIsLoading, setError]);

	/* We need this extra useEffect call for initial loading purposes */
	useEffect(() => {
		/* Initial load */
		setIsLoading(true);
		loadOrders().then(() => {
			setIsLoading(false);
		});
	}, [dispatch, loadOrders]);

	useEffect(() => {
		const unsubscribeFocusSub = props.navigation.addListener("focus", loadOrders);
		return () => {
			unsubscribeFocusSub();
		};
	}, [loadOrders]);

	if (isLoading) {
		return <LoadingSpinner />;
	}

	if (error) {
		return (
			<Fallback>
				<Text>An error occurred. Please try again.</Text>
				<View style={{ marginTop: 8 }}>
					<Button title="Try again" onPress={loadOrders} color={Colors.primaryColor} />
				</View>
			</Fallback>
		);
	}

	if (orders.length === 0) {
		return (
			<Fallback>
				<Text>No orders found. Let's start ordering some products.</Text>
			</Fallback>
		);
	}

	return (
		<FlatList
			onRefresh={loadOrders}
			refreshing={isRefreshing}
			data={orders}
			keyExtractor={(item) => item.id}
			renderItem={(itemData) => (
				<OrderItem
					orderTotalAmount={itemData.item.totalAmount}
					orderDate={itemData.item.readableOrderDate}
					orderItems={itemData.item.items}
				/>
			)}
		/>
	);
};

export const screenOptions = (navigationData) => {
	return {
		headerTitle: "Your Orders",
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Menu"
					iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
					onPress={() => {
						navigationData.navigation.toggleDrawer();
					}}
				/>
			</HeaderButtons>
		),
	};
};

export default OrderScreen;
