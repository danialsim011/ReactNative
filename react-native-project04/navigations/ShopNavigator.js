import React from "react";
import { SafeAreaView, Button, View, Platform, StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator, DrawerItemList } from "@react-navigation/drawer";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch } from "react-redux";

import Colors from "../constants/Colors";
import ProductOverviewScreen, {
	screenOptions as productOverviewScreenOptions,
} from "../screens/shop/ProductOverviewScreen";
import ProductDetailScreen, {
	screenOptions as productDetailScreenOptions,
} from "../screens/shop/ProductDetailScreen";
import CartScreen, { screenOptions as cartScreenOptions } from "../screens/shop/CartScreen";
import OrderScreen, { screenOptions as orderScreenOptions } from "../screens/shop/OrderScreen";
import UserProductScreen, {
	screenOptions as userProductScreenOptions,
} from "../screens/user/UserProductScreen";
import ModifyProductScreen, {
	screenOptions as modifyProductScreenOptions,
} from "../screens/user/ModifyProductScreen";
import AuthScreen, { screenOptions as authScreenOptions } from "../screens/user/AuthScreen";
import SplashScreen from "../screens/SplashScreen";
import * as authActions from "../store/actions/auth-actions";

const defaultNavOptions = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? Colors.primaryColor : "",
	},
	headerTitleStyle: {
		fontFamily: "open-sans-bold",
	},
	headerBackTitleStyle: {
		fontFamily: "open-sans",
	},
	headerTintColor: Platform.OS === "android" ? "white" : Colors.primaryColor,
};

const ProductStackNavigator = createStackNavigator();

export const ProductsNavigator = () => {
	return (
		<ProductStackNavigator.Navigator screenOptions={defaultNavOptions}>
			<ProductStackNavigator.Screen
				name="ProductOverview"
				component={ProductOverviewScreen}
				options={productOverviewScreenOptions}
			/>
			<ProductStackNavigator.Screen
				name="ProductDetail"
				component={ProductDetailScreen}
				options={productDetailScreenOptions}
			/>
			<ProductStackNavigator.Screen
				name="Cart"
				component={CartScreen}
				options={cartScreenOptions}
			/>
		</ProductStackNavigator.Navigator>
	);
};

const OrdersStackNavigator = createStackNavigator();

export const OrdersNavigator = () => {
	return (
		<OrdersStackNavigator.Navigator screenOptions={defaultNavOptions}>
			<OrdersStackNavigator.Screen
				name="Order"
				component={OrderScreen}
				options={orderScreenOptions}
			/>
		</OrdersStackNavigator.Navigator>
	);
};

const UserStackNavigator = createStackNavigator();

export const UserNavigator = () => {
	return (
		<UserStackNavigator.Navigator screenOptions={defaultNavOptions}>
			<UserStackNavigator.Screen
				name="UserProduct"
				component={UserProductScreen}
				options={userProductScreenOptions}
			/>
			<UserStackNavigator.Screen
				name="ModifyProduct"
				component={ModifyProductScreen}
				options={modifyProductScreenOptions}
			/>
		</UserStackNavigator.Navigator>
	);
};

const ShopDrawerNavigator = createDrawerNavigator();

export const ShopNavigator = () => {
	const dispatch = useDispatch();
	return (
		<ShopDrawerNavigator.Navigator
			drawerContent={(props) => {
				return (
					<View style={styles.drawer}>
						<SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
							<DrawerItemList {...props} />
							<View style={styles.drawerLogoutBtnContainer}>
								<Button
									title="Logout"
									color={Colors.primaryColor}
									onPress={() => {
										dispatch(authActions.logoutUser());
									}}
								/>
							</View>
						</SafeAreaView>
					</View>
				);
			}}
			drawerContentOptions={{
				activeTintColor: Colors.primaryColor,
			}}
		>
			<ShopDrawerNavigator.Screen
				name="Products"
				component={ProductsNavigator}
				options={{
					drawerIcon: (props) => (
						<Ionicons
							name={Platform.OS === "android" ? "md-cart" : "ios-cart"}
							size={24}
							color={props.color}
						/>
					),
				}}
			/>
			<ShopDrawerNavigator.Screen
				name="Orders"
				component={OrdersNavigator}
				options={{
					drawerIcon: (props) => (
						<Ionicons
							name={Platform.OS === "android" ? "md-list" : "ios-list"}
							size={24}
							color={props.color}
						/>
					),
				}}
			/>
			<ShopDrawerNavigator.Screen
				name="User"
				component={UserNavigator}
				options={{
					drawerIcon: (props) => (
						<Ionicons
							name={Platform.OS === "android" ? "md-create" : "ios-create"}
							size={24}
							color={props.color}
						/>
					),
				}}
			/>
		</ShopDrawerNavigator.Navigator>
	);
};

const AuthStackNavigator = createStackNavigator();

export const AuthNavigator = () => {
	return (
		<AuthStackNavigator.Navigator screenOptions={defaultNavOptions}>
			<AuthStackNavigator.Screen
				name="Auth"
				component={AuthScreen}
				options={authScreenOptions}
			/>
		</AuthStackNavigator.Navigator>
	);
};

const styles = StyleSheet.create({
	drawer: {
		flex: 1,
		paddingHorizontal: 16,
		paddingVertical: 32,
	},
	drawerLogoutBtnContainer: {
		marginTop: 8,
	},
});
