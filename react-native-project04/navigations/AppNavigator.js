import React from "react";
import { useSelector } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";

import { ShopNavigator, AuthNavigator } from "./ShopNavigator";
import SplashScreen from "../screens/SplashScreen";

const AppNavigator = (props) => {
	const isAuthenticated = useSelector((state) => !!state.auth.userToken);
	const didTryAutoLogin = useSelector((state) => state.auth.didTryAutoLogin);

	return <NavigationContainer>
		{isAuthenticated && <ShopNavigator />}
		{!isAuthenticated && didTryAutoLogin && <AuthNavigator />}
		{!isAuthenticated && !didTryAutoLogin && <SplashScreen />}
	</NavigationContainer>;
};

export default AppNavigator;
