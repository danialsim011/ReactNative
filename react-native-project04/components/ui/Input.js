import React, { useReducer, useEffect } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";

const INPUT_CHANGED = "INPUT_CHANGED";
const INPUT_BLURRED = "INPUT_BLURRED";

const inputReducer = (state, action) => {
	switch (action.type) {
		case INPUT_CHANGED:
			return {
				...state,
				inputValue: action.enteredValue,
				isInputValid: action.isInputValid,
			};
		case INPUT_BLURRED:
			return {
				...state,
				isTouched: true,
			};
		default:
			return state;
	}
};

const Input = (props) => {
	const [inputState, dispatchInput] = useReducer(inputReducer, {
		inputValue: props.initialValue ? props.initialValue : "",
		isInputValid: props.initialValidity,
		isTouched: false,
	});

	const { onInputChanged, id } = props;

	useEffect(() => {
		if (inputState.isTouched) {
			onInputChanged(id, inputState.inputValue, inputState.isInputValid);
		}
	}, [inputState, onInputChanged, id]);

	const inputChangedHandler = (enteredValue) => {
		const emailRegex =
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		let isInputValid = true;
		if (props.required && enteredValue.trim().length === 0) {
			isInputValid = false;
		}
		if (props.email && !emailRegex.test(enteredValue.toLowerCase())) {
			isInputValid = false;
		}
		if (props.min != null && +enteredValue < props.min) {
			isInputValid = false;
		}
		if (props.max != null && +enteredValue > props.max) {
			isInputValid = false;
		}
		if (props.minLength != null && enteredValue.length < props.minLength) {
			isInputValid = false;
		}
		dispatchInput({
			type: INPUT_CHANGED,
			enteredValue: enteredValue,
			isInputValid: isInputValid,
		});
	};

	const lostFocusHandler = () => {
		dispatchInput({
			type: INPUT_BLURRED,
		});
	};

	return (
		<View style={styles.formControl}>
			<Text style={styles.label}>{props.labelName}</Text>
			<TextInput
				{...props}
				style={styles.input}
				value={inputState.inputValue}
				onChangeText={inputChangedHandler}
				onBlur={lostFocusHandler}
			/>
			{!inputState.isInputValid && inputState.isTouched && (
				<View style={styles.errorContainer}>
					<Text style={styles.errorMessage}>{props.errorMessage}</Text>
				</View>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	formControl: {
		width: "100%",
	},
	label: {
		fontFamily: "open-sans-bold",
		marginVertical: 8,
	},
	input: {
		paddingHorizontal: 4,
		paddingVertical: 4,
		borderBottomColor: "#ccc",
		borderBottomWidth: 1,
	},
	errorContainer: {
		marginVertical: 2,
	},
	errorMessage: {
		fontFamily: "open-sans",
		color: "red",
		fontSize: 12,
	},
});

export default Input;
