import React, { useState } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import Colors from "../../constants/Colors";
import Card from "../ui/Card";

import CartItem from "./CartItem";

const OrderItem = (props) => {
	const [isShowingDetails, setIsShowingDetails] = useState(false);

	return (
		<Card style={styles.orderItem}>
			<View style={styles.orderSummary}>
				<Text style={styles.orderTotalAmount}>${props.orderTotalAmount.toFixed(2)}</Text>
				<Text style={styles.orderDate}>{props.orderDate}</Text>
			</View>
			<Button
				color={Colors.primaryColor}
				title={isShowingDetails ? "Hide Details" : "Show Details"}
				onPress={() => {
					setIsShowingDetails((prevState) => !prevState);
				}}
			/>
			{isShowingDetails && (
				<View style={styles.orderDetailItems}>
					{props.orderItems.map((cartItem) => (
						<CartItem
							key={cartItem.productId}
							cartItemQuantity={cartItem.quantity}
							cartItemTitle={cartItem.productTitle}
							cartItemSubtotal={cartItem.subtotal}
						/>
					))}
				</View>
			)}
		</Card>
	);
};

const styles = StyleSheet.create({
	orderItem: {
		margin: 20,
		padding: 12,
		alignItems: "center",
	},
	orderSummary: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
		marginBottom: 16,
	},
	orderTotalAmount: {
		fontFamily: "open-sans-bold",
		fontSize: 16,
	},
	orderDate: {
		fontFamily: "open-sans",
		fontSize: 16,
		color: "#888",
	},
	orderDetailItems: {
		width: "100%",
	},
});

export default OrderItem;
