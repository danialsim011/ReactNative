import React from "react";
import {
	TouchableOpacity,
	TouchableNativeFeedback,
	View,
	Text,
	StyleSheet,
	Platform,
} from "react-native";

const CategoryGridTile = (props) => {
	let TouchableComponent = TouchableOpacity;
	if (Platform.OS === "android" && Platform.Version >= 21) {
		TouchableComponent = TouchableNativeFeedback;
	}
	return (
		<View style={styles.gridItem}>
			<TouchableComponent onPress={props.onSelect} style={{ flex: 1 }}>
				<View style={{ ...styles.tileContainer, ...{ backgroundColor: props.color } }}>
					<Text style={styles.tileTitle} numberOfLines={2}>
						{props.title}
					</Text>
				</View>
			</TouchableComponent>
		</View>
	);
};

const styles = StyleSheet.create({
	gridItem: {
		flex: 1,
		margin: 8,
		height: 160,
		borderRadius: 12,
		overflow: Platform.OS === "android" && Platform.Version >= 21 ? "hidden" : "visible",
		elevation: 8,
		shadowColor: "black",
		shadowOffset: { width: 0, height: 2 },
		shadowRadius: 12,
		shadowOpacity: 0.30,
	},
	tileContainer: {
		flex: 1,
		borderRadius: 12,
		padding: 12,
		justifyContent: "flex-end",
		alignItems: "flex-end",
	},
	tileTitle: {
		fontFamily: "open-sans-bold",
		fontSize: 18,
		textAlign: "right",
	},
});

export default CategoryGridTile;
