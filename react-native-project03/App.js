import React, { useState } from "react";
import * as Font from "expo-font";
import AppLoading from "expo-app-loading";
import { enableScreens } from "react-native-screens";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";

import MealsNavigator from "./navigation/MealsNavigator";
import mealsReducer from "./store/reducers/meals";

enableScreens();

const rootReducer = combineReducers({
	meals: mealsReducer,
});

const store = createStore(rootReducer);

const fetchFonts = () => {
	return Font.loadAsync({
		"open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
		"open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
	});
};

export default function App() {
	const [isFontLoaded, setIsFontLoaded] = useState(false);

	if (!isFontLoaded) {
		return (
			<AppLoading
				startAsync={fetchFonts}
				onFinish={() => setIsFontLoaded(true)}
				onError={(err) => console.log(err)}
			/>
		);
	}

	return (
		<Provider store={store}>
			<MealsNavigator />
		</Provider>
	);
}
