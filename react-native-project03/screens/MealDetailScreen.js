import React, { useEffect, useCallback } from "react";
import { View, ScrollView, Text, StyleSheet, Image } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";

import CustomHeaderButton from "../components/HeaderButton";
import DefaultText from "../components/DefaultText";
import { toggleFavourite } from "../store/actions/meals";

const ListItem = (props) => {
	return (
		<View style={styles.listItem}>
			<DefaultText>{props.children}</DefaultText>
		</View>
	);
};

const MealDetailScreen = (props) => {
	const mealId = props.navigation.getParam("mealId");
	const availableMeals = useSelector(state => state.meals.meals);
	const selectedMeal = availableMeals.find((meal) => meal.id === mealId);
	const isFavouriteMeal = useSelector(state => state.meals.favouriteMeals.some(meal => meal.id === mealId));

	const dispatch = useDispatch();

	const toggleFavouriteHandler = useCallback(() => {
		dispatch(toggleFavourite(mealId));
	}, [dispatch, mealId]);

	useEffect(() => {
		props.navigation.setParams({toggleFav: toggleFavouriteHandler});
	}, [toggleFavouriteHandler]);

	useEffect(() => {
		props.navigation.setParams({isFavourite: isFavouriteMeal});
	}, [isFavouriteMeal]);

	return (
		<ScrollView>
			<Image source={{ uri: selectedMeal.imageUrl }} style={styles.image} />
			<View style={styles.details}>
				<DefaultText>{selectedMeal.duration} min.</DefaultText>
				<DefaultText>{selectedMeal.complexity.toUpperCase()}</DefaultText>
				<DefaultText>{selectedMeal.affordability.toUpperCase()}</DefaultText>
			</View>
			<Text style={styles.title}>Ingredients</Text>
			{selectedMeal.ingredients.map((ingredient) => (
				<ListItem key={ingredient}>{ingredient}</ListItem>
			))}
			<Text style={styles.title}>Steps</Text>
			{selectedMeal.steps.map((step) => (
				<ListItem key={step}>{step}</ListItem>
			))}
		</ScrollView>
	);
};

MealDetailScreen.navigationOptions = (navigationData) => {
	const mealTitle = navigationData.navigation.getParam("mealTitle");
	const toggleFavouriteFn = navigationData.navigation.getParam("toggleFav");
	const isFavouriteMeal = navigationData.navigation.getParam("isFavourite");

	return {
		headerTitle: mealTitle,
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
				<Item title="Favourite" iconName={isFavouriteMeal ? "ios-star" : "ios-star-outline"} onPress={toggleFavouriteFn} />
			</HeaderButtons>
		),
	};
};

const styles = StyleSheet.create({
	image: {
		width: "100%",
		height: 240,
	},
	details: {
		flexDirection: "row",
		padding: 16,
		justifyContent: "space-around",
	},
	title: {
		fontFamily: "open-sans-bold",
		fontSize: 18,
		textAlign: "center",
	},
	listItem: {
		marginVertical: 8,
		marginHorizontal: 16,
		borderColor: "#ccc",
		borderWidth: 1,
		padding: 8,
	},
});

export default MealDetailScreen;
