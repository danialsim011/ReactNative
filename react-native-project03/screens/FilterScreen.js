import React, { useState, useEffect, useCallback } from "react";
import { View, StyleSheet, Text, Switch, Platform } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useDispatch } from "react-redux";

import HeaderButton from "../components/HeaderButton";
import Colors from "../constants/Colors";
import { setFilters } from "../store/actions/meals";

const FilterSwitch = (props) => {
	return (
		<View style={styles.filterContainer}>
			<Text>{props.label}</Text>
			<Switch
				trackColor={{
					true: Colors.primaryColor,
				}}
				thumbColor={Platform.OS === "android" ? Colors.primaryColor : ""}
				value={props.state}
				onValueChange={props.onChange}
			/>
		</View>
	);
};

const FilterScreen = (props) => {
	const { navigation } = props;
	const dispatch = useDispatch();

	const [isGlutenFree, setIsGlutenFree] = useState(false);
	const [isLactoseFree, setIsLactoseFree] = useState(false);
	const [isVegan, setIsVegan] = useState(false);
	const [isVegetarian, setIsVegetarian] = useState(false);

	const saveFilters = useCallback(() => {
		const appliedFilters = {
			glutenFree: isGlutenFree,
			lactoseFree: isLactoseFree,
			vegan: isVegan,
			vegetarian: isVegetarian,
		};
		dispatch(setFilters(appliedFilters));
	}, [isGlutenFree, isLactoseFree, isVegetarian, isVegan]);

	useEffect(() => {
		navigation.setParams({
			save: saveFilters,
		});
	}, [saveFilters]);

	return (
		<View style={styles.screen}>
			<Text style={styles.title}>Available Filters / Restrictions</Text>
			<FilterSwitch
				label="Gluten-Free"
				state={isGlutenFree}
				onChange={(newVal) => setIsGlutenFree(newVal)}
			/>
			<FilterSwitch
				label="Lactose-Free"
				state={isLactoseFree}
				onChange={(newVal) => setIsLactoseFree(newVal)}
			/>
			<FilterSwitch label="Vegan" state={isVegan} onChange={(newVal) => setIsVegan(newVal)} />
			<FilterSwitch
				label="Vegetarian"
				state={isVegetarian}
				onChange={(newVal) => setIsVegetarian(newVal)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		alignItems: "center",
	},
	title: {
		fontFamily: "open-sans-bold",
		fontSize: 18,
		margin: 16,
		textAlign: "center",
	},
	filterContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "80%",
		marginVertical: 12,
	},
});

FilterScreen.navigationOptions = (navigationData) => {
	return {
		headerTitle: "Filter Meals",
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Menu"
					iconName="ios-menu"
					onPress={() => {
						navigationData.navigation.toggleDrawer();
					}}
				/>
			</HeaderButtons>
		),
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Save"
					iconName="ios-save"
					onPress={navigationData.navigation.getParam("save")}
				/>
			</HeaderButtons>
		),
	};
};

export default FilterScreen;
