import React from "react";
import { View, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import MealList from "../components/MealList";
import HeaderButton from "../components/HeaderButton";
import DefaultText from "../components/DefaultText";

const FavouriteScreen = (props) => {
	const favouriteMeals = useSelector((state) => state.meals.favouriteMeals);

	if (favouriteMeals.length === 0 || !favouriteMeals) {
		return (
			<View style={styles.fallback}>
				<DefaultText>No favourite meals found. Start adding some.</DefaultText>
			</View>
		);
	}

	return <MealList mealListData={favouriteMeals} navigation={props.navigation} />;
};

FavouriteScreen.navigationOptions = (navigationData) => {
	return {
		headerTitle: "Your Favourites",
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Menu"
					iconName="ios-menu"
					onPress={() => {
						navigationData.navigation.toggleDrawer();
					}}
				/>
			</HeaderButtons>
		),
	};
};

const styles = StyleSheet.create({
	fallback: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default FavouriteScreen;
