import React, { useState } from "react";
import { StyleSheet, View, Button, FlatList } from "react-native";

import GoalItem from "./components/GoalItem";
import GoalInput from "./components/GoalInput";

export default function App() {
	const [goalsList, setGoalsList] = useState([]);
	const [isModalDisplayed, setIsModalDisplayed] = useState(false);

	const addGoalHandler = (inputtedGoal) => {
		setGoalsList((currentGoalsList) => [
			...currentGoalsList,
			{ goalId: (Math.random() * 10).toString(), value: inputtedGoal },
		]);
		setIsModalDisplayed(false);
	};

	const removeGoalHandler = (goalId) => {
		setGoalsList((currentGoalsList) => {
			return currentGoalsList.filter((goal) => goal.goalId !== goalId);
		});
	};

	const cancelGoalAdditionHandler = () => {
		setIsModalDisplayed(false);
	};

	return (
		<View style={styles.screen}>
			<Button title="Add New Goal" onPress={() => setIsModalDisplayed(true)} />
			<GoalInput
				visibility={isModalDisplayed}
				onAddGoal={addGoalHandler}
				onCancel={cancelGoalAdditionHandler}
			/>
			<FlatList
				style={styles.list}
				keyExtractor={(item, index) => item.goalId}
				data={goalsList}
				renderItem={(itemData) => (
					<GoalItem
						onDelete={removeGoalHandler}
						goalId={itemData.item.goalId}
						title={itemData.item.value}
					/>
				)}
			></FlatList>
		</View>
	);
}

const styles = StyleSheet.create({
	screen: {
		paddingTop: 40,
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 16,
	},
	list: {
		marginTop: 8,
	},
});
