import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const GoalItem = (props) => {
	return (
		<TouchableOpacity activeOpacity="0.75" onPress={props.onDelete.bind(this, props.goalId)}>
			<View style={styles.listItem}>
				<Text>{props.title}</Text>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	listItem: {
		padding: 12,
		marginVertical: 4,
		backgroundColor: "#ccc",
		borderColor: "black",
		borderWidth: 1,
	}
});

export default GoalItem;
